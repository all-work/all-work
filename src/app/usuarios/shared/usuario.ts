export class Usuario {
    nome: string;
    email: string;
    cpf: number;
    nascimento: string;
    telefone: number;
    senha: string;
    eLocatario: boolean = false;
}
