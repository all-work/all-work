import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class UsuarioService {

  constructor(private db: AngularFireDatabase, private http: HttpClient) { }

  insert(usuario: Usuario) {
    // this.db.list('usuarios').push(usuario)
    //   .then((result: any) => {
    //     console.log(result.key);
    //   });

    this.http.get(`${environment.baseUrl}user/teste`)
      .toPromise()
      .then((res: any) => {
        console.log("res", res)
      })
      .catch((err: any) => {
        console.log("err", err)
      })
  }

  update(usuario: Usuario, key: string) {
    this.db.list('usuarios').update(key, usuario)
      .catch((error: any) => {
        console.log(error.key);
      });
  }

  getAll() {
    return this.db.list('usuarios')
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        })
      );
  }

  delete(key: string) {
    this.db.object(`usuarios/${key}`).remove();
  }
}
