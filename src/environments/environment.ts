// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: "http://localhost:8080/",
  firebase: {
    apiKey: "AIzaSyAAHl8BK232jZ206FXIqms_wuhLrjmfh80",
    authDomain: "allwork-333.firebaseapp.com",
    databaseURL: "https://allwork-333.firebaseio.com",
    projectId: "allwork-333",
    storageBucket: "allwork-333.appspot.com",
    messagingSenderId: "763646329135"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
